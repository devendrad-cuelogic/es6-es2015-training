# Module : ECMAScript 2015 A.K.A. ES6

## Objective : 
To learn and understand new features introduced in JavaScript language under the ECMAScript 2015 specs also know as ES6.

## Pre-requisites : 
Should have good knowledge of JavaScript and or can go through the training for the same found at [Module: JavaScript Basics](http://www.w3schools.com/js/ "JavaScript Basics")

___

### Topic : Scope and block bindings

### Theory/Material :
Read [Block Bindings](https://leanpub.com/understandinges6/read/#leanpub-auto-block-bindings)<br>
Watch videos 01-05 from _*[ES6 For Everyone](https://es6.io/)*_

### Evaluation :

##### Objective Test(s) :
1. What are two new types of declarations introduced in ES6 ?
2. Can `let` declaration variables be mutated ?
3. Can `const` declaration variables be mutated ?
4. Let declarations are hoisted ?
5. Const declarations are hoisted ?
6. `const` declaration in case of object, does it make the obeject immutable ? 

##### Subjective Test(s) :
1. What is variable hoisting ?
2. What is lexical scoping ?
3. What is Temporal Dead Zone ?
4. Difference between `let` and `const` ? 
5. Explain with example `let` and `const` varibale behaviour inside `for..loop`
6. Explain with example _*Global Block Bindings*_ behaviour of `let` and `const`

___

### Topic : Arrow Functions

### Theory/Material : 
Read [Arrow Functions](https://leanpub.com/understandinges6/read/#leanpub-auto-arrow-functions)<br>
Watch videos 06-08, 10-11 from _*[ES6 For Everyone](https://es6.io/)*_


### Evaluation : 

#### Objective Test(s) : 
1. Can there be named arrow functions ?
2. Which is the symbol used for arrow functions ?
3. Does arrow functions have `prototype` property ?
4. Arrow functions can be used as contrcutors ?

#### Subjective Test(s) : 
1. List down various possible arrow functions syntax 
2. How implicit return works in Arrow functions
3. Whats the Gotcha in case of while implicitly returning object literals from arrow function ?
4. Behaviour of arrow functions in case of `this`
5. How `arguments` binding work for arrow functions
6. Explaing arrow functions behaviour in case of call(), apply(), and bind() 

___

### Topic : String templates, tagged templates & new String methods

### Theory/Material : 
Read [Template Literals](https://leanpub.com/understandinges6/read/#leanpub-auto-template-literals)<br>
Watch videos 12-17 from _*[ES6 For Everyone](https://es6.io/)*_


### Evaluation : 

#### Objective Test(s) : 
1. What is string interpolation / literal substitution ?
2. Can you substitute function calls inside template strings ? 
3. Does template literals support multiline strings ?
4. How many parameters the new String methods _*startsWith*_ accept ? 
5. Does new string methods listed below accept regular expression as input ?
6. What are these `` called ?

#### Subjective Test(s) : 
1. What are tagged templates and its use case ?
2. Write a program to show use of tagged template
3. Write a program to show use of following new String methods
    * `includes()`
    * `startsWith()`
    * `endsWith()`

___


### Topic : Destructuring Objects, Arrays and function retun values

### Theory/Material : 
Read [Destructuring for Easier Data Access](https://leanpub.com/understandinges6/read/#leanpub-auto-destructuring-for-easier-data-access)<br>
Watch videos 18-21 from _*[ES6 For Everyone](https://es6.io/)*_


### Evaluation : 

#### Objective Test(s) : 
1. Does Destructuring syntax work without initial values ?
2. Can we use Destructuring syntax while calling a function ?
3. What values is asseigned to variable in destructuring syntax when there is no corresponding value in assigned object ?
4. Can nested objects be destructured ? 

#### Subjective Test(s) : 
1. What is Object Destructuring ?
2. Explain _*destructuring with assignment*_ using example
3. Explaing how to use different local variable names in destructuring 
4. Explain the use of destructuring for variable swapping with example  
5. What is Array Destructuring ?
6. Explain how _*Nested Destructuring*_ works in case of Arrays, with example.

___

### Topic : for of loop and Array methods

### Theory/Material :
Read [Dummy Title](https://localhost)<br>
Watch videos 22-27 from _*[ES6 For Everyone](https://es6.io/)*_

### Evaluation :

##### Objective Test(s) :
1. for-of loop can not be used with ?
2. for-of loop can `break` out of loop on condition ?
3. `continue` can be used inside for-of loop ?
4. Does for-of loop give index of the item inside loop ?
5. Strings are iterables in ES6 ?
6. New array methods `.from` and `.of` are defined on prototype of `Array` ?


##### Subjective Test(s) :
1. What are limitation or gotchas with for loop, forEach loop and for-in loop ?
2. Gotchas with for-in loop and custom prototype methods.
3. Make use of array `entries()` and array destructuring to get index and value inside for-of loop .
4. Iterator over string's each character using for-of loop
5. Iterate over dom-collection using for-of loop
6. Using `map()` method iterate over dom-collection.
7. Make use of new array methods `.find()` and `.findIndex()` ?
8. Make use of array methods `.some()` and `.every()` ?

___

### Topic : Spread and Rest operator, Object literal upgrades

### Theory/Material :
Read [Dummy Title](https://localhost)<br>
Watch videos 28-33 from _*[ES6 For Everyone](https://es6.io/)*_

### Evaluation :

##### Objective Test(s) :
1. Can we use spread opeator with objects ?
2. Can we use multiple rest parameters in function argument list ?
3. Are Object literal notation and JSON exactly same ?

##### Subjective Test(s) :
1. Make use of spread(`...`) operator with `.apply()` method
2. User spread(`...`) operator to concat/merge arrays
3. Copy an array into an array using `...` operator
4. Use spread (`...`) operator to make array of characters from given string
5. `Array.from()`  Vs. spread (`...`) operator for converting dom-collection to array.
6. Use spread operator with `Array.push()` to add elements to array from an array
7. Rest params
8. Difference with Rest and Spread params
9. Make use of rest(`...`) operator in destructuring
10. Make use of Shorthand property names, Shorthand method names, Computed property names
11. Make object from two arrays considering one as keys arrays and second as values array

___

### Topic : ES6 Promises

### Theory/Material :
Read [Dummy Title](https://localhost)<br>
Watch videos 34-37 from _*[ES6 For Everyone](https://es6.io/)*_

### Evaluation :

##### Objective Test(s) :
1. What are the states of a Promise ?
2. What is the difference between `.then()` and `.catch()` ?

##### Subjective Test(s) :
1. Lifecycle of a promise;
2. Write a program using promise to fetch data from an API, and handle succcess and failure cases.
3. Chain promises to fetch data from API
4. Make use of `Promise.all()`

___

### Topic : Javascript modules

### Theory/Material :
Read [Dummy Title](https://localhost)<br>
Watch videos 34-37 from _*[ES6 For Everyone](https://es6.io/)*_

### Evaluation :

##### Objective Test(s) :
1. What are two ways to export something from a module ?
2. What are diffrent ways of refering a module while importing from them 
3. 
4. 
5. 
6. 

##### Subjective Test(s) :
1. What are Modules in javascript and why to use them ?
2. Understand how to use CommonJS Modules and AMD ( Asynchronous Module Definition ) 
3.
4.
5.
6.


___

### Topic : Classes

### Theory/Material :
Read [Dummy Title](https://localhost)<br>
Watch videos 51-54 from _*[ES6 For Everyone](https://es6.io/)*_

### Evaluation :

##### Objective Test(s) :
1. What are two ways of using class in ES6 ?

##### Subjective Test(s) :
1. Declare a generic class with contructor and few methods and create objects out of it and use it.
2. Declare a generic class and create static methods on it and make use of it.
3. Declare a generic class and declare a sub-class and make use of `super()` 
4. Create and use setters and getters on a class.
5. Extend built-in classes and make use of it.


___

### Topic : Generators

### Theory/Material :
Read [Dummy Title](https://localhost)<br>
Watch videos 55-57 from _*[ES6 For Everyone](https://es6.io/)*_

### Evaluation :

##### Objective Test(s) :
1. 
2. 
3. 
4. 
5. 
6. 

##### Subjective Test(s) :
1.
2.
3.
4.
5.
6.

___

### Topic : Maps, Weak Maps, Sets, Weak Sets

### Theory/Material :
Read [Dummy Title](https://localhost)<br>
Watch videos 61-66 from _*[ES6 For Everyone](https://es6.io/)*_

### Evaluation :

##### Objective Test(s) :
1. Can we add duplicate items in set ? 
2. Are set items index based ? 
3. Can we craete set with existing array ?
4. Can we add primitive types to weak sets ?
5. Can we loop through weak sets ?

##### Subjective Test(s) :
1. Write a program to make use of various set methods like `.add()`, `.has()`, `.delete()`, `.clear()`
2. Make use of set with generators to iterator over its iterms
3. Difference between sets and maps
4. Write a program to make use of various map methods like `.set()`, `.has()`, `.get()`
5. Loop through maps using `forEach` and `for-of` loop syntax
6. Make use of maps as associative arrays 
7. Make use of WeakMap 
